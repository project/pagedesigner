<?php

namespace Drupal\pagedesigner\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Pagedesigner type entities.
 */
interface ElementTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}

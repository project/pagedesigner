<?php

namespace Drupal\pagedesigner\Entity;

use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\UserInterface;

/**
 * Defines the Pagedesigner Element entity.
 *
 * @ingroup pagedesigner
 *
 * @ContentEntityType(
 *   id = "pagedesigner_element",
 *   label = @Translation("Pagedesigner Element"),
 *   bundle_label = @Translation("Pagedesigner type"),
 *   handlers = {
 *     "storage" = "Drupal\pagedesigner\ElementStorage",
 *     "view_builder" = "Drupal\pagedesigner\ElementViewBuilder",
 *     "list_builder" = "Drupal\pagedesigner\ElementListBuilder",
 *     "views_data" = "Drupal\pagedesigner\Entity\ElementViewsData",
 *     "translation" = "Drupal\pagedesigner\ElementTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\pagedesigner\Form\ElementForm",
 *       "add" = "Drupal\pagedesigner\Form\ElementForm",
 *       "edit" = "Drupal\pagedesigner\Form\ElementForm",
 *       "delete" = "Drupal\pagedesigner\Form\ElementDeleteForm",
 *     },
 *     "access" = "Drupal\pagedesigner\ElementAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\pagedesigner\ElementHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "pagedesigner_element",
 *   data_table = "pagedesigner_element_field_data",
 *   revision_table = "pagedesigner_element_revision",
 *   revision_data_table = "pagedesigner_element_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer pagedesigner element entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message",
 *   },
 *   links = {
 *     "canonical" = "/admin/content/pagedesigner_element/{pagedesigner_element}",
 *     "add-page" = "/admin/content/pagedesigner_element/add",
 *     "add-form" = "/admin/content/pagedesigner_element/add/{pagedesigner_type}",
 *     "edit-form" = "/admin/content/pagedesigner_element/{pagedesigner_element}/edit",
 *     "delete-form" = "/admin/content/pagedesigner_element/{pagedesigner_element}/delete",
 *     "version-history" = "/admin/content/pagedesigner_element/{pagedesigner_element}/revisions",
 *     "revision" = "/admin/content/pagedesigner_element/{pagedesigner_element}/revisions/{pagedesigner_element_revision}/view",
 *     "revision_revert" = "/admin/content/pagedesigner_element/{pagedesigner_element}/revisions/{pagedesigner_element_revision}/revert",
 *     "revision_delete" = "/admin/content/pagedesigner_element/{pagedesigner_element}/revisions/{pagedesigner_element_revision}/delete",
 *     "translation_revert" = "/admin/content/pagedesigner_element/{pagedesigner_element}/revisions/{pagedesigner_element_revision}/revert/{langcode}",
 *     "collection" = "/admin/content/pagedesigner_elements",
 *   },
 *   bundle_entity_type = "pagedesigner_type",
 *   field_ui_base_route = "entity.pagedesigner_type.edit_form"
 * )
 */
class Element extends RevisionableContentEntityBase implements ElementInterface {

  use EntityChangedTrait;

  /**
   * Undocumented variable.
   *
   * @var [type]
   */
  protected $handlerType = '';

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly,
    // make the pagedesigner_element owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTagsToInvalidate() {
    if ($this->isNew()) {
      return [];
    }
    $tags = [$this->entityTypeId . ':' . $this->id() . ':internal'];
    if ($this->isPublished()) {
      $tags[] = $this->entityTypeId . ':' . $this->id() . ':public';
    }
    return $tags;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return [$this->entityTypeId . ':' . $this->id()];
  }

  /**
   * {@inheritdoc}
   */
  public function getHandlerType() {
    if (!empty($this->handlerType)) {
      return $this->handlerType;
    }
    return $this->bundle();
  }

  /**
   * {@inheritdoc}
   */
  public function setHandlerType($type) {
    $this->handlerType = $type;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function saveEdit() {
    $this->setPublished(FALSE);
    $this->setNewRevision(TRUE);
    $this->save();
  }

  /**
   * {@inheritdoc}
   */
  public function loadNewestPublished() {
    $entity = $this;
    $langcode = $entity->language()->getId();
    $col = \Drupal::database()->query(
          'SELECT vid FROM {pagedesigner_element_field_revision} WHERE id=:nid AND status = 1 AND COALESCE(deleted,0) =  0 and langcode LIKE :langcode ORDER BY vid DESC',
          [
            ':nid' => $this->id(),
            ':langcode' => $langcode,
          ]
      )
      ->fetchCol();
    if (count($col) == 0) {
      return NULL;
    }
    $vid = reset($col);
    if ($vid != $this->getRevisionId()) {
      /** @var Element $entity */
      $entity = \Drupal::entityTypeManager()
        ->getStorage($this->entityTypeId)
        ->loadRevision($vid);
      if ($entity->hasTranslation($langcode)) {
        $entity = $entity->getTranslation($langcode);
      }
    }
    $entity->setHandlerType($this->getHandlerType());
    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Pagedesigner Element entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Pagedesigner Element entity.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 500,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'region' => 'hidden',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'text_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE)
      ->setRequired(FALSE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Pagedesigner Element is published.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(TRUE)
      ->setTranslatable(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    $fields['entity'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Entity'))
      ->setDescription(t('The entity this elements belongs to.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'node')
      ->setSetting('handler', 'default')
      ->setCardinality(1)
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => -200,
        'cardinality' => '-1',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
          'cardinality' => '-1',
        ],
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['container'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Container'))
      ->setDescription(t('The container of this element.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'pagedesigner_element')
      ->setSetting('handler', 'default')
      ->setCardinality(1)
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => -100,
        'cardinality' => '-1',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
          'cardinality' => '-1',
        ],
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['parent'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Parent'))
      ->setDescription(t('The parent of this element.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'pagedesigner_element')
      ->setSetting('handler', 'default')
      ->setCardinality(1)
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => -100,
        'cardinality' => '-1',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
          'cardinality' => '-1',
        ],
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['children'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Children'))
      ->setDescription(t('The children of this element.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'pagedesigner_element')
      ->setSetting('handler', 'default')
      ->setCardinality(-1)
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => -100,
        'cardinality' => '-1',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
          'cardinality' => '-1',
        ],
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['deleted'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Is this element deleted'))
      ->setDescription(t('Indicates if the element is deleted.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ])
      ->setReadOnly(FALSE)
      ->setRevisionable(TRUE)
      ->setTranslatable(FALSE);

    return $fields;
  }

}

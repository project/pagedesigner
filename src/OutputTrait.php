<?php

namespace Drupal\pagedesigner;

/**
 * Trait for classes that have an output.
 */
trait OutputTrait {
  /**
   * The output.
   *
   * @var mixed
   */
  protected $output = NULL;

  /**
   * Returns the output of the last operation.
   *
   * @return mixed
   *   The output of the last operation.
   */
  public function getOutput() {
    return $this->output;
  }

}

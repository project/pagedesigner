<?php

namespace Drupal\pagedesigner;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Interface for the pagedesigner service implementation.
 */
interface PagedesignerServiceInterface {

  /**
   * Get all pagedesigner fields of the entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to get the fields from.
   *
   * @return array
   *   The list of fields, keyed by field name.
   */
  public function getPagedesignerFields(ContentEntityInterface $entity);

  /**
   * Enable translations on the field.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to delete the elements for.
   * @param string $field_name
   *   (Optional) The field to delete the elements for.
   */
  public function enableTranslation(ContentEntityInterface &$entity, string $field_name = NULL);

  /**
   * Add the container for a field.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to delete the elements for.
   * @param string $field_name
   *   (Optional) The field to delete the elements for.
   */
  public function addContainerForField(ContentEntityInterface &$entity, string $field_name);

  /**
   * Mark pagedesigner elements as deleted.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to delete the elements for.
   * @param string $field_name
   *   (Optional) The field to delete the elements for.
   */
  public function clearPagedesigner(ContentEntityInterface &$entity, string $field_name = NULL);

  /**
   * Loads/creates the container for the given entity in the default language.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity being rendered.
   * @param string $field_name
   *   The pagedesigner field name.
   *
   * @return \Drupal\pagedesigner\Entity\Element
   *   The container.
   */
  public function getContainer(ContentEntityInterface $entity, $field_name = 'field_pagedesigner_content');

  /**
   * Create a new container.
   *
   * @param \Drupal\pagedesigner\ContentEntityInterface $entity
   *   The entity for which to create the container.
   *
   * @return \Drupal\pagedesigner\Entity\Element
   *   The container.
   */
  public function createContainer(ContentEntityInterface $entity);

  /**
   * Whether assets should be loaded for the pagedesigner.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity being loaded.
   * @param \Drupal\Core\Session\AccountInterface $user
   *   The user loading the entity.
   *
   * @return bool
   *   True if assets should be loaded, false otherwise.
   */
  public function isEditMode(ContentEntityInterface $entity = NULL, AccountInterface $user = NULL);

  /**
   * Whether the current route is the pagedesigner edit route.
   *
   * @return bool
   *   True if the pagedesigner edit route is active, false otherwise.
   */
  public function isPagedesignerRoute();

  /**
   * Returns true if the given entity may be edited by the user.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to check.
   * @param \Drupal\Core\Session\AccountInterface $user
   *   The user to check access for.
   *
   * @return bool
   *   True if the user has access to the entity, false otherwise.
   */
  public function hasEditAccess(ContentEntityInterface $entity = NULL, AccountInterface $user = NULL);

  /**
   * Get the element handler.
   *
   * @return Drupal\pagedesigner\Service\ElementHandler
   *   Return the pagedesigner element handler.
   */
  public function getHandler();

  /**
   * Get the module handler.
   *
   * @return \Drupal\Core\Extension\ModuleHandlerInterface
   *   The module handler.
   */
  public function getModuleHandler();

}

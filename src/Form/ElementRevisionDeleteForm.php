<?php

namespace Drupal\pagedesigner\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting a Pagedesigner Element revision.
 *
 * @ingroup pagedesigner
 */
class ElementRevisionDeleteForm extends ConfirmFormBase {


  /**
   * The Pagedesigner Element revision.
   *
   * @var \Drupal\pagedesigner\Entity\ElementInterface
   */
  protected $revision;

  /**
   * The Pagedesigner Element storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $elementStorage;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructs a new ElementRevisionDeleteForm.
   *
   *   The entity storage.
   *   The database connection.
   */
  public function __construct(EntityStorageInterface $entity_storage, Connection $connection) {
    $this->elementStorage = $entity_storage;
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $entity_manager = $container->get('entity_type.manager');
    return new static(
      $entity_manager->getStorage('pagedesigner_element'),
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pagedesigner_element_revision_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t(
      'Are you sure you want to delete the revision from %revision-date?',
      [
        '%revision-date' => \Drupal::service('date.formatter')->format($this->revision->getRevisionCreationTime()),
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url(
      'entity.pagedesigner_element.version_history',
      [
        'pagedesigner_element' => $this->revision->id(),
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $pagedesigner_element_revision = NULL) {
    $this->revision = $this->elementStorage->loadRevision($pagedesigner_element_revision);
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->elementStorage->deleteRevision($this->revision->getRevisionId());

    $this->logger('content')->notice(
      'Pagedesigner Element: deleted %title revision %revision.',
      [
        '%title' => $this->revision->label(),
        '%revision' => $this->revision->getRevisionId(),
      ]
    );
    \Drupal::messenger()->addMessage(
      $this->t(
        'Revision from %revision-date of Pagedesigner Element %title has been deleted.',
        [
          '%revision-date' => \Drupal::service('date.formatter')->format($this->revision->getRevisionCreationTime()),
          '%title' => $this->revision->label(),
        ]
      )
    );
    $form_state->setRedirect(
      'entity.pagedesigner_element.canonical',
       ['pagedesigner_element' => $this->revision->id()]
    );
    if ($this->connection->query(
      'SELECT COUNT(DISTINCT vid) FROM {pagedesigner_element_field_revision} WHERE id = :id',
      [
        ':id' => $this->revision->id(),
      ])->fetchField() > 1) {
      $form_state->setRedirect(
        'entity.pagedesigner_element.version_history',
         ['pagedesigner_element' => $this->revision->id()]
      );
    }
  }

}

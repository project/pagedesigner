<?php

namespace Drupal\pagedesigner\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Pagedesigner Element entities.
 *
 * @ingroup pagedesigner
 */
class ElementDeleteForm extends ContentEntityDeleteForm {


}

<?php

namespace Drupal\pagedesigner\Form;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\pagedesigner\Entity\ElementInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for reverting a Pagedesigner Element revision.
 *
 * @ingroup pagedesigner
 */
class ElementRevisionRevertForm extends ConfirmFormBase {


  /**
   * The Pagedesigner Element revision.
   *
   * @var \Drupal\pagedesigner\Entity\ElementInterface
   */
  protected $revision;

  /**
   * The Pagedesigner Element storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $elementStorage;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructs a new ElementRevisionRevertForm.
   *
   *   The Pagedesigner Element storage.
   *   The date formatter service.
   */
  public function __construct(EntityStorageInterface $entity_storage, DateFormatterInterface $date_formatter) {
    $this->elementStorage = $entity_storage;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')->getStorage('pagedesigner_element'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pagedesigner_element_revision_revert_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to revert to the revision from %revision-date?', ['%revision-date' => $this->dateFormatter->format($this->revision->getRevisionCreationTime())]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.pagedesigner_element.version_history', ['pagedesigner_element' => $this->revision->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Revert');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $pagedesigner_element_revision = NULL) {
    $this->revision = $this->elementStorage->loadRevision($pagedesigner_element_revision);
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // The revision timestamp will be updated when the revision is saved. Keep
    // the original one for the confirmation message.
    $original_revision_timestamp = $this->revision->getRevisionCreationTime();

    $this->revision = $this->prepareRevertedRevision($this->revision, $form_state);
    $this->revision->revision_log = $this->t('Copy of the revision from %date.', ['%date' => $this->dateFormatter->format($original_revision_timestamp)]);
    $this->revision->save();

    $this->logger('content')->notice(
      'Pagedesigner Element: reverted %title revision %revision.',
      [
        '%title' => $this->revision->label(),
        '%revision' => $this->revision->getRevisionId(),
      ]
    );
    \Drupal::messenger()->addMessage(
      $this->t('Pagedesigner Element %title has been reverted to the revision from %revision-date.',
      [
        '%title' => $this->revision->label(),
        '%revision-date' => $this->dateFormatter->format($original_revision_timestamp),
      ])
    );
    $form_state->setRedirect(
      'entity.pagedesigner_element.version_history',
      ['pagedesigner_element' => $this->revision->id()]
    );
  }

  /**
   * Prepares a revision to be reverted.
   *
   *   The revision to be reverted.
   *   The current state of the form.
   *
   * @return \Drupal\pagedesigner\Entity\ElementInterface
   *   The prepared revision ready to be stored.
   */
  protected function prepareRevertedRevision(ElementInterface $revision, FormStateInterface $form_state) {
    $revision->setNewRevision();
    $revision->isDefaultRevision(TRUE);
    $revision->setRevisionCreationTime(\Drupal::time()->getRequestTime());

    return $revision;
  }

}

<?php

namespace Drupal\pagedesigner;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for pagedesigner_element.
 */
class ElementTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}

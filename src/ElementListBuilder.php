<?php

namespace Drupal\pagedesigner;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Defines a class to build a listing of Pagedesigner Element entities.
 *
 * @ingroup pagedesigner
 */
class ElementListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [];
    $header['id'] = $this->t('Pagedesigner Element ID');
    $header['type'] = $this->t('Type');
    $header['parent'] = $this->t('Parent');
    $header['container'] = $this->t('Container');
    $header['entity'] = $this->t('Entity');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row = [];
    $row['id'] = $entity->id();
    $row['type'] = $entity->type->getValue()[0]['target_id'];
    if (isset($entity->parent->target_id) && !empty($entity->parent->target_id)) {
      $row['parent'] = $entity->parent->target_id;
    }
    else {
      $row['parent'] = "-";
    }
    if ($entity->container->target_id && !empty($entity->container->target_id)) {
      $row['container'] = $entity->container->target_id;
    }
    else {
      $row['container'] = "-";
    }
    if ($entity->entity->target_id && !empty($entity->entity->target_id)) {
      $row['entity'] = $entity->entity->target_id;
    }
    else {
      $row['entity'] = "-";
    }
    return $row + parent::buildRow($entity);
  }

}

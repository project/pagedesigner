<?php

namespace Drupal\pagedesigner\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event that is fired when a pagedesigner is handled.
 */
class ElementEvent extends Event {
  /**
   * The event name.
   *
   * @var string
   */
  protected $eventName = '';

  /**
   * The event data.
   *
   * @var mixed
   */
  protected $data = NULL;

  /**
   * Constructs the event.
   *
   * @param string $eventName
   *   The event name.
   * @param mixed $data
   *   The event data.
   */
  public function __construct(string $eventName, mixed &$data) {
    $this->eventName = $eventName;
    $this->data = &$data;
  }

  /**
   * Returns the event name.
   *
   * @return string
   *   The event name.
   */
  public function getEventName() {
    return $this->eventName;
  }

  /**
   * Returns the event data.
   *
   * @return mixed
   *   The event data.
   */
  public function &getData() {
    return $this->data;
  }

}

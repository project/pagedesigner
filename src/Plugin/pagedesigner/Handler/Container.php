<?php

namespace Drupal\pagedesigner\Plugin\pagedesigner\Handler;

use Drupal\pagedesigner\ElementViewBuilder;
use Drupal\pagedesigner\Entity\Element;

/**
 * Process entities of type "container".
 *
 * @PagedesignerHandler(
 *   id = "container",
 *   name = @Translation("Container render"),
 *   types = {
 *     "container",
 *   },
 * )
 */
class Container extends Cell {

  /**
   * {@inheritdoc}
   */
  public function view(Element $entity, string $view_mode, array &$build = []) {
    parent::view($entity, $view_mode, $build);
    if ($view_mode == ElementViewBuilder::RENDER_MODE_EDIT) {
      $build['#prefix'] = '<section data-grapes-block="content" data-gjs-draggable="false" data-gjs-droppable="true" data-entity-id="' . $entity->id() . '" data-gjs-type="container" class="pd-content pd-edit container-fluid">';
      $build['#suffix'] = '</section>';
    }
    else {
      $build['#prefix'] = '<section class="pd-content pd-live container-fluid">';
      $build['#suffix'] = '</section>';
    }
  }

}

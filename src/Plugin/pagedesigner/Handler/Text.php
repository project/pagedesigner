<?php

namespace Drupal\pagedesigner\Plugin\pagedesigner\Handler;

use Drupal\Component\Utility\Xss;
use Drupal\pagedesigner\Entity\Element;

/**
 * Process entities of type "text".
 *
 * @PagedesignerHandler(
 *   id = "text",
 *   name = @Translation("Text renderer"),
 *   types = {
 *     "text",
 *   },
 * )
 */
class Text extends Content {

  /**
   * {@inheritdoc}
   */
  public function get(Element $entity, string &$result = '') {
    $result .= Xss::filter((string) $entity->field_content->value);
  }

}

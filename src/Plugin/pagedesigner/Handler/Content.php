<?php

namespace Drupal\pagedesigner\Plugin\pagedesigner\Handler;

use Drupal\pagedesigner\Entity\Element;
use Drupal\pagedesigner\Plugin\FieldHandlerBase;

/**
 * Process entities of type "content".
 *
 * @PagedesignerHandler(
 *   id = "content",
 *   name = @Translation("Content renderer"),
 *   types = {
 *     "content"
 *   },
 * )
 */
class Content extends FieldHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function generate($definition, array $data, Element &$entity = NULL) {
    parent::generate(['type' => 'content', 'name' => $definition['type']], $data, $entity);
  }

}

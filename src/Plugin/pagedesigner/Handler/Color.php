<?php

namespace Drupal\pagedesigner\Plugin\pagedesigner\Handler;

/**
 * Process entities of type "color".
 *
 * @PagedesignerHandler(
 *   id = "color",
 *   name = @Translation("Color handler"),
 *   types = {
 *     "color",
 *   },
 * )
 */
class Color extends Content {

}

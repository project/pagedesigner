<?php

namespace Drupal\pagedesigner\Plugin\pagedesigner\Handler;

use Drupal\pagedesigner\Entity\Element;
use Drupal\pagedesigner\Plugin\PlainFieldHandlerBase;

/**
 * Process entities of type "checkbox".
 *
 * @PagedesignerHandler(
 *   id = "checkbox",
 *   name = @Translation("Checkbox renderer"),
 *   types = {
 *     "checkbox",
 *     "toggle",
 *   },
 * )
 */
class Checkbox extends PlainFieldHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function generate($definition, array $data, Element &$entity = NULL) {
    parent::generate(['type' => 'content', 'name' => $definition['type']], $data, $entity);
  }

  /**
   * {@inheritdoc}
   */
  public function build(Element $entity, string $view_mode, array &$build = []) {
    $value = $this->elementHandler->get($entity);
    $build = ['#plain_text' => !!$value] + $build;
  }

}

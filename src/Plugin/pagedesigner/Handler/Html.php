<?php

namespace Drupal\pagedesigner\Plugin\pagedesigner\Handler;

use Drupal\pagedesigner\ElementViewBuilder;
use Drupal\pagedesigner\Entity\Element;
use Drupal\pagedesigner\Plugin\HandlerConfigTrait;

/**
 * Process entities of type "html".
 *
 * @PagedesignerHandler(
 *   id = "html",
 *   name = @Translation("Html renderer"),
 *   types = {
 *     "html"
 *   },
 * )
 */
class Html extends Content {

  // Import config property and setter, auto set in HandlerConfigTrait::create.
  use HandlerConfigTrait;

  /**
   * {@inheritdoc}
   */
  public function serialize(Element $entity, array &$result = []) {
    $result = [
      (string) $entity->field_content->value,
    ] + $result;
  }

  /**
   * {@inheritdoc}
   */
  public function get(Element $entity, string &$result = '') {
    $build = [];
    $this->build($entity, ElementViewBuilder::RENDER_MODE_INTERNAL, $build);
    $result .= $this->renderer->renderPlain($build);
  }

  /**
   * {@inheritdoc}
   */
  public function build(Element $entity, string $view_mode, array &$build = []) {
    $format = $this->configFactory->get('pagedesigner.settings')->get('filter_format');
    if (empty($format)) {
      $format = 'plain_text';
    }
    $build = [
      '#type' => 'processed_text',
      '#text' => (string) $entity->field_content->value,
      '#format' => $format,
      '#filter_types_to_skip' => [],
      '#langcode' => $entity->langcode->value,
    ];
  }

}

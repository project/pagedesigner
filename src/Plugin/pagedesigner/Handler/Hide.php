<?php

namespace Drupal\pagedesigner\Plugin\pagedesigner\Handler;

use Drupal\pagedesigner\Entity\Element;
use Drupal\pagedesigner\Plugin\PlainFieldHandlerBase;

/**
 * Process entities of type "hide".
 *
 * @PagedesignerHandler(
 *   id = "hide",
 *   name = @Translation("Hide handler"),
 *   types = {
 *      "hide"
 *   },
 * )
 */
class Hide extends PlainFieldHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function serialize(Element $entity, array &$result = []) {
    $result = [];
  }

  /**
   * {@inheritdoc}
   */
  public function get(Element $entity, string &$result = '') {
    $result = '';
  }

  /**
   * {@inheritdoc}
   */
  public function patch(Element $entity, array $data) {
  }

  /**
   * {@inheritdoc}
   */
  public function generate($definition, array $data, Element &$entity = NULL) {
    parent::generate(['type' => 'content', 'name' => 'hide'], $data, $entity);
  }

}

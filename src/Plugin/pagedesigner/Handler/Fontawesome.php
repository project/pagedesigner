<?php

namespace Drupal\pagedesigner\Plugin\pagedesigner\Handler;

use Drupal\pagedesigner\Entity\Element;
use Drupal\pagedesigner\Plugin\PlainFieldHandlerBase;

/**
 * Process entities of type "fontawesome".
 *
 * @PagedesignerHandler(
 *   id = "fontawesome",
 *   name = @Translation("Fontawesome renderer"),
 *   types = {
 *     "fontawesome",
 *   },
 * )
 */
class Fontawesome extends PlainFieldHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function generate($definition, array $data, Element &$entity = NULL) {
    parent::generate(['type' => 'content', 'name' => $definition['type']], $data, $entity);
  }

}

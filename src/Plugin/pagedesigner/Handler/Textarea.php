<?php

namespace Drupal\pagedesigner\Plugin\pagedesigner\Handler;

use Drupal\pagedesigner\Entity\Element;
use Drupal\pagedesigner\Plugin\HandlerConfigTrait;

/**
 * Process entities of type "textarea".
 *
 * @PagedesignerHandler(
 *   id = "textarea",
 *   name = @Translation("Textarea renderer"),
 *   types = {
 *     "textarea"
 *   },
 * )
 */
class Textarea extends Longtext {

  // Import config property and setter, auto set in HandlerConfigTrait::create.
  use HandlerConfigTrait;

  /**
   * {@inheritdoc}
   */
  public function build(Element $entity, string $view_mode, array &$build = []) {
    $value = $this->elementHandler->get($entity);
    $format = $this->configFactory->get('pagedesigner.settings')->get('filter_format_textarea');
    if (empty($format)) {
      $format = 'plain_text';
    }
    $build = [
      '#type' => 'processed_text',
      '#text' => (string) $value,
      '#format' => $format,
      '#filter_types_to_skip' => [],
      '#langcode' => $entity->langcode->value,
    ] + $build;
  }

}

<?php

namespace Drupal\pagedesigner\Plugin\pagedesigner\Handler;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\pagedesigner\Entity\Element;
use Drupal\pagedesigner\Plugin\PlainFieldHandlerBase;
use Drupal\ui_patterns\Definition\PatternDefinitionField;

/**
 * Process entities of type "select".
 *
 * @PagedesignerHandler(
 *   id = "select",
 *   name = @Translation("Select processor"),
 *   types = {
 *      "select",
 *   }
 * )
 */
class Select extends PlainFieldHandlerBase {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function prepare(PatternDefinitionField &$field, array &$fieldArray) {
    parent::prepare($field, $fieldArray);
    $fieldArray['options'] = [];
    if (!empty($field->toArray()['options'])) {
      foreach ($field->toArray()['options'] as $key => $option) {
        if (is_string($option)) {
          $option = $this->t($option);
        }
        $fieldArray['options'][$key] = $option;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function generate($definition, array $data, Element &$entity = NULL) {
    parent::generate(['type' => 'content', 'name' => 'select'], $data, $entity);
  }

}

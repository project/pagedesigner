<?php

namespace Drupal\pagedesigner\Plugin\pagedesigner\Asset;

use Drupal\pagedesigner\Plugin\AssetPluginBase;

/**
 * Povides the standard handler for media assets.
 *
 * @PagedesignerAsset(
 *   id = "standard",
 *   name = @Translation("Default asset"),
 *   types = {
 *      "none",
 *   },
 * )
 */
class Standard extends AssetPluginBase {

}

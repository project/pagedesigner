<?php

namespace Drupal\pagedesigner\Plugin\pagedesigner;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * {@inheritdoc}
 */
interface AssetPluginInterface extends PluginInspectionInterface, ContainerFactoryPluginInterface {

}

<?php

namespace Drupal\pagedesigner\Plugin;

use Drupal\pagedesigner\Entity\Element;

/**
 * Povides the plain field handler base class pagedesigner elements.
 *
 * The field handler provides methods for handling
 * plain trait pagedesigner elements (e.g. select, href etc.)
 * It is to be extended and the inherited class
 * to be annotated for the correct type of pagedesiger element.
 */
class PlainFieldHandlerBase extends FieldHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function build(Element $entity, string $view_mode, array &$build = []) {
    $value = $this->elementHandler->get($entity);
    $build = ['#plain_text' => ''] + $build;
    if (!empty($value) || strlen($value)) {
      $build['#plain_text'] = $value;
    }
  }

}

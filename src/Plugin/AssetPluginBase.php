<?php

namespace Drupal\pagedesigner\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\pagedesigner\Plugin\pagedesigner\AssetPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * {@inheritdoc}
 */
abstract class AssetPluginBase extends PluginBase implements AssetPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
          $configuration,
          $plugin_id,
          $plugin_definition
      );
  }

}

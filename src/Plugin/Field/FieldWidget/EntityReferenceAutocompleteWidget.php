<?php

namespace Drupal\pagedesigner\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteWidget as OrigEntityReferenceAutocompleteWidget;

/**
 * Plugin implementation of the 'entity_reference_autocomplete' widget.
 *
 * @FieldWidget(
 *   id = "pagedesigner_entity_reference_autocomplete",
 *   label = @Translation("Autocomplete"),
 *   description = @Translation("An autocomplete text field."),
 *   field_types = {
 *     "pagedesigner_item"
 *   }
 * )
 */
class EntityReferenceAutocompleteWidget extends OrigEntityReferenceAutocompleteWidget {

}

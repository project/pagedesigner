<?php

namespace Drupal\pagedesigner\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\pagedesigner\PagedesignerService;
use Drupal\pagedesigner\Service\Renderer;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the Pagedesigner content formatter.
 *
 * @FieldFormatter(
 *   id = "pagedesigner_formatter",
 *   module = "pagedesigner",
 *   label = @Translation("Pagedesigner content"),
 *   field_types = {
 *     "pagedesigner_item"
 *   }
 * )
 */
class PagedesignerFormatter extends FormatterBase {

  /**
   * Constructs a StringFormatter instance.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\pagedesigner\PagedesignerService $pagedesignerService
   *   The pagedesigner service.
   * @param \Drupal\pagedesigner\Service\Renderer $pagedesignerRenderer
   *   The pagedesigner renderer.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    protected PagedesignerService $pagedesignerService,
    protected Renderer $pagedesignerRenderer,
    protected AccountInterface $currentUser,
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('pagedesigner.service'),
      $container->get('pagedesigner.service.renderer'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Displays the pagedesigner content.');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $node = $items->getEntity();
    if ($node != NULL && $node instanceof ContentEntityInterface) {
      $node = $node->getTranslation($langcode);
      foreach ($items as $item) {
        $container = $item->entity;
        if ($container != NULL && $container->hasTranslation($langcode)) {
          $container = $container->getTranslation($langcode);
          if ($this->pagedesignerService->isPagedesignerRoute()) {
            $this->pagedesignerRenderer->renderForEdit($container, $node);
          }
          elseif ($this->currentUser->hasPermission('view unpublished pagedesigner element entities')) {
            $this->pagedesignerRenderer->render($container, $node);
          }
          else {
            $this->pagedesignerRenderer->renderForPublic($container, $node);
          }
          $elements[] = $this->pagedesignerRenderer->getOutput();
        }
      }
    }
    return $elements;
  }

}

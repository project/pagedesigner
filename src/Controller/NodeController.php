<?php

namespace Drupal\pagedesigner\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\node\Controller\NodeViewController;

/**
 * Controller for accessing the pagedesigner route.
 */
class NodeController extends NodeViewController {

  /**
   * {@inheritdoc}
   */
  public function view(EntityInterface $node, $view_mode = 'full', $langcode = NULL) {
    $build = parent::view($node, $view_mode, $langcode);
    // Set render context to prevent cache from reusing canonical render.
    $build['#cache']['contexts'][] = 'route';
    return $build;
  }

}

<?php

namespace Drupal\pagedesigner\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\pagedesigner\Entity\ElementInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ElementController.
 *
 *  Returns responses for Pagedesigner Element routes.
 */
class ElementController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Constructs an ElementController object.
   *
   * @param \Drupal\Core\Datetime\DateFormatterInterface $dateFormatter
   *   The date formatter service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(protected DateFormatterInterface $dateFormatter, protected RendererInterface $renderer) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('date.formatter'),
      $container->get('renderer')
    );
  }

  /**
   * Displays a Pagedesigner Element  revision.
   *
   * @param int $pagedesigner_element_revision
   *   The Pagedesigner Element  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($pagedesigner_element_revision) {
    $pagedesigner_element = $this->entityTypeManager()->getStorage('pagedesigner_element')->loadRevision($pagedesigner_element_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('pagedesigner_element');

    return $view_builder->view($pagedesigner_element);
  }

  /**
   * Page title callback for a Pagedesigner Element  revision.
   *
   * @param int $pagedesigner_element_revision
   *   The Pagedesigner Element  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($pagedesigner_element_revision) {
    /** @var \Drupal\pagedesigner\ElementStorage $pagedesigner_element_storage */
    $pagedesigner_element_storage = $this->entityTypeManager()->getStorage('pagedesigner_element');
    /** @var \Drupal\pagedesigner\Entity\ElementInterface $pagedesigner_element */
    $pagedesigner_element = $pagedesigner_element_storage->loadRevision($pagedesigner_element_revision);
    return $this->t(
      'Revision of %title from %date',
      [
        '%title' => $pagedesigner_element->label(),
        '%date' => $this->dateFormatter->format($pagedesigner_element->getRevisionCreationTime()),
      ]
    );
  }

  /**
   * Generates an overview table of older revisions of a Pagedesigner Element .
   *
   *   A Pagedesigner Element  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(ElementInterface $pagedesigner_element) {
    $build = [];
    $account = $this->currentUser();
    $langcode = $pagedesigner_element->language()->getId();
    $langname = $pagedesigner_element->language()->getName();
    $languages = $pagedesigner_element->getTranslationLanguages();
    $has_translations = ((is_countable($languages) ? count($languages) : 0) > 1);
    /** @var \Drupal\pagedesigner\ElementStorage $pagedesigner_element_storage */
    $pagedesigner_element_storage = $this->entityTypeManager()->getStorage('pagedesigner_element');

    $build['#title'] = $has_translations ?
      $this->t(
        '@langname revisions for %title',
        [
          '@langname' => $langname,
          '%title' => $pagedesigner_element->label(),
        ]
      ) :
      $this->t(
        'Revisions for %title',
        [
          '%title' => $pagedesigner_element->label(),
        ]
      );
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all pagedesigner element revisions") || $account->hasPermission('administer pagedesigner element entities')));
    $delete_permission = (($account->hasPermission("delete all pagedesigner element revisions") || $account->hasPermission('administer pagedesigner element entities')));

    $rows = [];

    $vids = $pagedesigner_element_storage->revisionIds($pagedesigner_element);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\pagedesigner\ElementInterface $revision */
      $revision = $pagedesigner_element_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $pagedesigner_element->getRevisionId()) {
          $link = Link::fromTextAndUrl(
            $date,
            new Url(
              'entity.pagedesigner_element.revision',
              [
                'pagedesigner_element' => $pagedesigner_element->id(),
                'pagedesigner_element_revision' => $vid,
              ]
            )
          );
        }
        else {
          $link = $pagedesigner_element->toLink($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link->toString(),
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute(
                'entity.pagedesigner_element.translation_revert',
                [
                  'pagedesigner_element' => $pagedesigner_element->id(),
                  'pagedesigner_element_revision' => $vid,
                  'langcode' => $langcode,
                ]
              ) :
              Url::fromRoute(
                'entity.pagedesigner_element.revision_revert',
                [
                  'pagedesigner_element' => $pagedesigner_element->id(),
                  'pagedesigner_element_revision' => $vid,
                ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute(
                'entity.pagedesigner_element.revision_delete', [
                  'pagedesigner_element' => $pagedesigner_element->id(),
                  'pagedesigner_element_revision' => $vid,
                ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['pagedesigner_element_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}

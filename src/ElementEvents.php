<?php

namespace Drupal\pagedesigner;

/**
 * List the events for element actions.
 */
final class ElementEvents {
  /**
   * Event fired before getting data from a pagedesigner element.
   */
  public const GET_BEFORE = 'pagedesigner.getBefore';

  /**
   * Event fired before getting data from a pagedesigner element.
   */
  public const GET_AFTER = 'pagedesigner.getAfter';

  /**
   * Event fired before patching a pagedesigner element.
   */
  public const PATCH_BEFORE = 'pagedesigner.patchBefore';

  /**
   * Event fired after patching a pagedesigner element.
   */
  public const PATCH_AFTER = 'pagedesigner.patchAfter';

  /**
   * Event fired before adjusting the attachment collection in a handler.
   */
  public const COLLECTATTACHMENTS_BEFORE = 'pagedesigner.collectAttachmentsBefore';

  /**
   * Event fired after adjusting the attachment collection in a handler.
   */
  public const COLLECTATTACHMENTS_AFTER = 'pagedesigner.collectAttachmentsAfter';

  /**
   * Event fired before collecting the patterns in a handler.
   */
  public const COLLECTPATTERNS_BEFORE = 'pagedesigner.collectPatternsBefore';

  /**
   * Event fired after collecting the patterns in a handler.
   */
  public const COLLECTPATTERNS_AFTER = 'pagedesigner.collectPatternsAfter';

  /**
   * Event fired before adapting the pattern list in a handler.
   */
  public const ADAPTPATTERNS_BEFORE = 'pagedesigner.adaptPatternsBefore';

  /**
   * Event fired after adapting the pattern list in a handler.
   */
  public const ADAPTPATTERNS_AFTER = 'pagedesigner.adaptPatternsAfter';

  /**
   * Event fired before preparing a field of a pattern.
   */
  public const PREPARE_BEFORE = 'pagedesigner.prepareBefore';

  /**
   * Event fired after preparing a field of a pattern.
   */
  public const PREPARE_AFTER = 'pagedesigner.prepareAfter';

  /**
   * Event fired before getting the content from a pagedesigner element.
   */
  public const GETCONTENT_BEFORE = 'pagedesigner.getContentBefore';

  /**
   * Event fired after getting the content from a pagedesigner element.
   */
  public const GETCONTENT_AFTER = 'pagedesigner.getContentAfter';

  /**
   * Event fired before creating a pagedesigner element.
   */
  public const GENERATE_BEFORE = 'pagedesigner.generateBefore';

  /**
   * Event fired after creating a pagedesigner element.
   */
  public const GENERATE_AFTER = 'pagedesigner.generateAfter';

  /**
   * Event fired before copying a pagedesigner element.
   */
  public const COPY_BEFORE = 'pagedesigner.copyBefore';

  /**
   * Event fired after copying a pagedesigner element.
   */
  public const COPY_AFTER = 'pagedesigner.copyAfter';

  /**
   * Event fired before deleting a pagedesigner element.
   */
  public const DELETE_BEFORE = 'pagedesigner.deleteBefore';

  /**
   * Event fired after deleting a pagedesigner element.
   */
  public const DELETE_AFTER = 'pagedesigner.deleteAfter';

  /**
   * Event fired before restoring a pagedesigner element.
   */
  public const RESTORE_BEFORE = 'pagedesigner.deleteBefore';

  /**
   * Event fired after restoring a pagedesigner element.
   */
  public const RESTORE_AFTER = 'pagedesigner.deleteAfter';

  /**
   * Event fired before viewing a pagedesigner element.
   */
  public const VIEW_BEFORE = 'pagedesigner.viewBefore';

  /**
   * Event fired after viewing a pagedesigner element.
   */
  public const VIEW_AFTER = 'pagedesigner.viewAfter';

  /**
   * Event fired before building a pagedesigner element.
   */
  public const BUILD_BEFORE = 'pagedesigner.buildBefore';

  /**
   * Event fired after building a pagedesigner element.
   */
  public const BUILD_AFTER = 'pagedesigner.buildAfter';

  /**
   * Event fired before serializing the data from a pagedesigner element.
   */
  public const SERIALIZE_BEFORE = 'pagedesigner.serializeBefore';

  /**
   * Event fired after serializing the data from a pagedesigner element.
   */
  public const SERIALIZE_AFTER = 'pagedesigner.serializeAfter';

  /**
   * Event fired before describing the structure from a pagedesigner element.
   */
  public const DESCRIBE_BEFORE = 'pagedesigner.describeBefore';

  /**
   * Event fired after describing the structure from a pagedesigner element.
   */
  public const DESCRIBE_AFTER = 'pagedesigner.describeAfter';

  /**
   * Event fired before publishing a pagedesigner element.
   */
  public const PUBLISH_BEFORE = 'pagedesigner.publishBefore';

  /**
   * Event fired after publishing a pagedesigner element.
   */
  public const PUBLISH_AFTER = 'pagedesigner.publishAfter';

  /**
   * Event fired before unpublishing a pagedesigner element.
   */
  public const UNPUBLISH_BEFORE = 'pagedesigner.unpublishBefore';

  /**
   * Event fired after unpublishing a pagedesigner element.
   */
  public const UNPUBLISH_AFTER = 'pagedesigner.unpublishAfter';

}

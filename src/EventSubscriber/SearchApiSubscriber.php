<?php

namespace Drupal\pagedesigner\EventSubscriber;

use Drupal\search_api\Event\MappingFieldTypesEvent;
use Drupal\search_api\Event\SearchApiEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscriber to react to search api events.
 */
class SearchApiSubscriber implements EventSubscriberInterface {

  /**
   * Add the pagedesigner DataType as a html field for field selection.
   *
   *   The mapping event provided by search api.
   */
  public function mapPagedesignerType(MappingFieldTypesEvent &$event) {
    $mapping = &$event->getFieldTypeMapping();
    $mapping['pagedesigner_item_data'] = 'search_api_html';
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [];
    $events[SearchApiEvents::MAPPING_FIELD_TYPES][] = [
      'mapPagedesignerType',
      256,
    ];
    return $events;
  }

}

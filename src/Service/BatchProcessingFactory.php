<?php

namespace Drupal\pagedesigner\Service;

use Drupal\pagedesigner\Service\BatchProcessing\FieldChangeBatch;

/**
 * Provides batch processing for pagedesigner.
 */
class BatchProcessingFactory {

  /**
   * Return a batch to enable translations on a field.
   *
   * @return \Drupal\pagedesigner\Service\BatchProcessing\FieldChangeBatch
   *   The batch to enable translations.
   */
  public function translationEnableOnField() {
    return new FieldChangeBatch('enableTranslation', 'Enable pagedesigner translations.');
  }

  /**
   * Return a batch to add containers for a field.
   *
   * @return \Drupal\pagedesigner\Service\BatchProcessing\FieldChangeBatch
   *   The batch to add containers.
   */
  public function addContainerForField() {
    return new FieldChangeBatch('addContainerForField', 'Add pagedesigner to entities.');
  }

  /**
   * Return a batch to clear containers on a field.
   *
   * @return \Drupal\pagedesigner\Service\BatchProcessing\FieldChangeBatch
   *   The batch to clear containers.
   */
  public function clearContainersOnField() {
    return new FieldChangeBatch('clearPagedesigner', 'Delete pagedesigner entities.');
  }

}

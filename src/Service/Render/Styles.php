<?php

namespace Drupal\pagedesigner\Service\Render;

/**
 * Styles container.
 *
 * This class provides methods to add and removes css style definitions.
 */
class Styles {

  /**
   * Contains the added styles.
   *
   * @var array
   */
  protected $styles = [];

  /**
   * Returns styles for the given key.
   *
   * @return string
   *   The generated css.
   */
  public function getStyles($key) {
    $output = '';
    if (!empty($this->styles[$key])) {
      foreach ($this->styles[$key] as $id => $style) {
        $context = ['key' => $key, 'id' => $id];
        \Drupal::service('module_handler')->alter('pagedesigner_css_statement', $style, $context);
        if (!empty($style)) {
          $output .= '#pd-cp-' . $id . '{ ' . $style . '}';
        }
      }
    }
    return $output;
  }

  /**
   * Add a style definition.
   *
   * @param string $key
   *   The size for which to add the style definition (mn, sm, xs).
   * @param string $style
   *   The style definition to add.
   * @param int $id
   *   The id of the element.
   */
  public function addStyle($key, $style, $id) {
    if (empty($this->styles[$key])) {
      $this->styles[$key] = [];
    }
    if (empty($this->styles[$key][$id])) {
      $this->styles[$key][$id] = '';
    }
    $this->styles[$key][$id] .= $style;
  }

}

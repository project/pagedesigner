<?php

namespace Drupal\pagedesigner\Service;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\pagedesigner\Entity\Element;

/**
 * Interface for the Pagedesigner StateChanger service.
 */
interface StateChangerInterface {

  /**
   * Publish an entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityBase|null $entity
   *   The entity to publish.
   *
   * @return self
   *   Returns itself for chaining.
   */
  public function publish(ContentEntityBase $entity = NULL);

  /**
   * Unpublish an entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityBase|null $entity
   *   The entity to unpublish.
   *
   * @return self
   *   Returns itself for chaining.
   */
  public function unpublish(ContentEntityBase $entity = NULL);

  /**
   * Copy the pagedesigner content of an entity.
   *
   * @param \Drupal\pagedesigner\Element $entity
   *   The element to copy.
   * @param \Drupal\pagedesigner\Element|null $container
   *   (Optional) The container for the copied entity.
   * @param \Drupal\pagedesigner\Element|null $parent
   *   (Optional) The parent for the copied entity.
   *
   * @return self
   *   Returns itself for chaining.
   */
  public function copy(Element $entity, Element $container = NULL, Element $parent = NULL);

  /**
   * Generate a container for an entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityBase|null $entity
   *   The entity to generate the container for.
   *
   * @return self
   *   Returns itself for chaining.
   */
  public function generate(ContentEntityBase $entity = NULL);

  /**
   * Delete an entity and its pagedesigner content.
   *
   * @param \Drupal\Core\Entity\ContentEntityBase|null $entity
   *   The entity to delete.
   *
   * @return self
   *   Returns itself for chaining.
   */
  public function delete(ContentEntityBase $entity = NULL);

  /**
   * Returns the output of the last operation.
   *
   * @return mixed
   *   The output of the last operation.
   */
  public function getOutput();

}

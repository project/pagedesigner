<?php

namespace Drupal\Tests\pagedesigner_embed\Kernel\HandlerTests;

use Drupal\media\Entity\Media;
use Drupal\Tests\pagedesigner_media\Kernel\HandlerTests\MediaHandlerTestBase;

/**
 * Test the "embed" handler with a predefined entity defintion.
 *
 * Tests serializing, getting and rendering of the entity.
 *
 * @group pagedesigner
 * @requires module video_embed_field
 */
class EmbedHandlerTest extends MediaHandlerTestBase {

  /**
   * Define the properties of the entity.
   *
   * @var array
   */
  protected $entityDefinition = [
    'type' => 'embed',
    'name' => 'embed',
    'langcode' => 'en',
    'field_media' => ['target_id' => 1],
  ];

  /**
   * Define handler to be tested.
   *
   * @var string
   */
  protected $handlerId = 'embed';

  /**
   * Test url.
   *
   * @var string
   */
  protected $testUrl = 'https://www.youtube.com/watch?v=3p4MZJsexEs';

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    self::$modules[] = 'video_embed_field';
    self::$modules[] = 'video_embed_media';
    self::$modules[] = 'pagedesigner_embed';

    parent::setUp();

    $this->installConfig(['pagedesigner_embed']);
  }

  /**
   * {@inheritdoc}
   */
  public function assertPreConditions(): void {
    parent::assertPreConditions();
    // Create media entity.
    $this->media = Media::create([
      'bundle' => $this->entityDefinition['type'],
      'field_embed_source' => ['value' => $this->testUrl],
    ]);
    $this->entity->field_media->entity = $this->media;
    $this->entityDefinition['field_media']['target_id'] = $this->media->id();
    $this->assertTrue($this->entity->field_media->entity == $this->media);
    $this->assertTrue($this->entityDefinition['field_media']['target_id'] == $this->media->id());
  }

  /**
   * {@inheritdoc}
   *
   * Additionally, assert the returned array contains the pagedesigner library.
   */
  protected function collectAttachmentsTest() {
    $attachments = parent::collectAttachmentsTest();
    $this->assertTrue(\in_array('pagedesigner_embed/pagedesigner', $attachments['library']));
    return $attachments;
  }

  /**
   * {@inheritdoc}
   *
   * Additionally, assert the returned value is equal to the file url.
   */
  protected function getTest() {
    $value = parent::getTest();
    $provider = \Drupal::service('video_embed_field.provider_manager')->loadProviderFromInput($this->testUrl);
    $renderArray = $provider->renderEmbedCode(0, 0, 0);
    $src = $renderArray['#url'];
    $this->assertTrue($value == $src);
    return $value;
  }

  /**
   * {@inheritdoc}
   *
   * Additionally, assert the returned values
   * are matching the media and the file.
   */
  protected function serializeTest() {
    $result = parent::serializeTest();
    $provider = \Drupal::service('video_embed_field.provider_manager')->loadProviderFromInput($this->testUrl);
    $renderArray = $provider->renderEmbedCode(0, 0, 0);
    $src = $renderArray['#url'];
    $this->assertTrue($result['src'] == $src);
    return $result;
  }

  /**
   * {@inheritdoc}
   *
   * Additionally, check the render array schema and content.
   */
  protected function internalBuildTest() {
    $build = parent::internalBuildTest();
    $provider = \Drupal::service('video_embed_field.provider_manager')->loadProviderFromInput($this->testUrl);
    $renderArray = $provider->renderEmbedCode(0, 0, 0);
    $src = $renderArray['#url'];
    $this->assertTrue($build['#plain_text'] == $src);
    return $build;
  }

}

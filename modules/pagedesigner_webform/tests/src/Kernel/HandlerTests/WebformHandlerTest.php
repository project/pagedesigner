<?php

namespace Drupal\Tests\pagedesigner_webform\Kernel\HandlerTests;

use Drupal\Tests\pagedesigner\Kernel\HandlerTests\CompoundHandlerTestBase;
use Drupal\webform\Entity\Webform;

/**
 * Test the "webform" handler with a predefined entity defintion.
 *
 * Tests serializing, getting and rendering of the entity.
 *
 * @group pagedesigner
 * @requires module webform
 */
class WebformHandlerTest extends CompoundHandlerTestBase {

  /**
   * Define the properties of the entity.
   *
   * @var array
   */
  protected $entityDefinition = [
    'type' => 'webform',
    'name' => 'webform',
    'langcode' => 'en',
    'field_webform' => ['target_id' => 'webform_test'],
  ];

  /**
   * Define handler to be tested.
   *
   * @var string
   */
  protected $handlerId = 'webform';

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    self::$modules[] = 'system';
    self::$modules[] = 'path';
    self::$modules[] = 'webform';
    self::$modules[] = 'pagedesigner_webform';
    self::$modules[] = 'path_alias';
    parent::setUp();
    $this->installEntitySchema('path_alias');
    $this->installSchema('webform', ['webform']);
  }

  /**
   * {@inheritdoc}
   */
  public function assertPreConditions(): void {
    parent::assertPreConditions();
    $webform = Webform::create(
      [
        'id' => $this->entityDefinition['field_webform']['target_id'],
      ]
    );
    $webform->save();
  }

  /**
   * {@inheritdoc}
   *
   * Additionally, assert the returned array contains the webform pattern.
   */
  protected function collectPatternsTest() {
    $patterns = parent::collectPatternsTest();
    $expectedPatternId = 'webform:' . $this->entityDefinition['field_webform']['target_id'];
    $this->assertTrue(!empty($patterns[$expectedPatternId]));
    $definition = $patterns[$expectedPatternId];
    $this->assertTrue($definition->getAdditional()['type'] == 'webform');
    $this->assertTrue(!empty($definition->getAdditional()['markup']));
    return $patterns;
  }

  /**
   * {@inheritdoc}
   *
   * Additionally, assert the returned array contains the pagedesigner library.
   */
  protected function collectAttachmentsTest() {
    $attachments = parent::collectAttachmentsTest();
    $this->assertTrue(\in_array('pagedesigner_webform/pagedesigner', $attachments['library']));
    return $attachments;
  }

  /**
   * {@inheritdoc}
   *
   * Additionally, assert the returned value is equal to the given value.
   */
  protected function serializeTest() {
    $result = parent::serializeTest();
    $this->assertTrue($result['webform_id'] == $this->entityDefinition['field_webform']['target_id']);
    return $result;
  }

  /**
   * {@inheritdoc}
   *
   * Additionally, assert the returned value is empty.
   */
  protected function getTest() {
    $value = parent::getTest();
    $this->assertTrue(empty($value));
    return $value;
  }

  /**
   * {@inheritdoc}
   *
   * Additionally, check the render array schema and content.
   */
  protected function internalBuildTest() {
    $build = parent::internalBuildTest();
    $this->assertTrue($build['pagedesigner_webform']['#webform_id'] == $this->entityDefinition['field_webform']['target_id']);
    return $build;
  }

}

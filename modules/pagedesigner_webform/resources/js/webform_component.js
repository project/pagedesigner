// components
(function ($, Drupal) {

  function component(editor) {

    // add new component type webform
    editor.DomComponents.addType('webform', {
      extend: 'component',
    });
  }

  Drupal.behaviors.pagedesigner_webform = {
    attach: function (context, settings) {
      $(document).on('pagedesigner-init-components', function (e, editor) {
        component(editor);
      });
    }
  };

})(jQuery, Drupal);

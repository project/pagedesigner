<?php

namespace Drupal\pagedesigner_yoast\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'keyword_formatter' formatter.
 *
 * @FieldFormatter(
 *    id = "pagedesigner_yoast_keyword",
 *    label = @Translation("Keyword formatter"),
 *    field_types = {
 *      "yoast_seo",
 *    }
 * )
 */
class KeywordFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#markup' => $item->focus_keyword,
      ];
    }

    return $elements;
  }

}

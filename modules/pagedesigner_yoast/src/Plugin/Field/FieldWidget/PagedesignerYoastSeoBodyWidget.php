<?php

namespace Drupal\pagedesigner_yoast\Plugin\Field\FieldWidget;

use Drupal\text\Plugin\Field\FieldWidget\TextareaWithSummaryWidget;

/**
 * Advanced widget for yoast_seo field.
 *
 * @FieldWidget(
 *   id = "pagedesigner_yoast_seo_body_widget",
 *   label = @Translation("Pagedesigner body field widget"),
 *   field_types = {
 *     "text_with_summary"
 *   }
 * )
 * @deprecated in pagedesigner:2.0.0-beta12 and is removed from pagedesigner:2.0.0.
 *   Use PagedesignerYoastSeoWidget instead.
 * @see https://www.drupal.org/project/pagedesigner/issues/3355494
 */
class PagedesignerYoastSeoBodyWidget extends TextareaWithSummaryWidget {

}

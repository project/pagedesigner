<?php

namespace Drupal\pagedesigner_video\Plugin\pagedesigner\Handler;

use Drupal\pagedesigner\Entity\Element;
use Drupal\pagedesigner_media\Plugin\MediaFieldHandlerBase;

/**
 * Process entities of type "video".
 *
 * @PagedesignerHandler(
 *   id = "video",
 *   name = @Translation("Video handler"),
 *   types = {
 *      "video"
 *   },
 * )
 */
class Video extends MediaFieldHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function collectAttachments(array &$attachments) {
    $attachments['library'][] = 'pagedesigner_video/pagedesigner';
    static::addMediaTypeInfo($attachments, 'video');
  }

  /**
   * {@inheritdoc}
   */
  public function generate($definition, array $data, Element &$entity = NULL) {
    parent::generate(['type' => 'video', 'name' => 'video'], $data, $entity);
  }

}

<?php

namespace Drupal\Tests\pagedesigner_gallery\Kernel\HandlerTests;

use Drupal\Tests\pagedesigner\Kernel\HandlerTests\HandlerTestBase;

/**
 * Test the "gallery" handler with a predefined entity defintion.
 *
 * Tests serializing, getting and rendering of the entity.
 *
 * @group pagedesigner
 */
class GalleryHandlerTest extends HandlerTestBase {

  /**
   * {@inheritdoc}
   */
  protected $handlerId = 'gallery';

  /**
   * {@inheritdoc}
   */
  protected $entityDefinition = [
    'type' => 'content',
    'name' => 'gallery',
    'langcode' => 'en',
    'field_content' => ['value' => '[]'],
  ];

  /**
   * {@inheritdoc}
   */
  protected $fieldDefinition = [
    'name' => 'gallery',
    'label' => 'gallery',
    'type' => 'gallery',
  ];

}

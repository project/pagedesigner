<?php

namespace Drupal\Tests\pagedesigner_gallery\Kernel\HandlerTests;

use Drupal\Tests\pagedesigner\Kernel\HandlerTests\HandlerTestBase;

/**
 * Test the "gallery_gallery" handler with a predefined entity defintion.
 *
 * Tests serializing, getting and rendering of the entity.
 *
 * @group pagedesigner
 */
class GalleryGalleryHandlerTest extends HandlerTestBase {

  /**
   * {@inheritdoc}
   */
  protected $handlerId = 'gallery_gallery';

  /**
   * {@inheritdoc}
   */
  protected $entityDefinition = [
    'type' => 'content',
    'name' => 'gallery_gallery',
    'langcode' => 'en',
    'field_content' => ['value' => '[]'],
  ];

  /**
   * {@inheritdoc}
   */
  protected $fieldDefinition = [
    'name' => 'gallery_gallery',
    'label' => 'gallery_gallery',
    'type' => 'gallery_gallery',
  ];

}

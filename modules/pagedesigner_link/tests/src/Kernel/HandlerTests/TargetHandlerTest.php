<?php

namespace Drupal\Tests\pagedesigner_link\Kernel\HandlerTests;

use Drupal\Tests\pagedesigner\Kernel\HandlerTests\PlainFieldHandlerTestBase;

/**
 * Test the "target" handler with a predefined entity defintion.
 *
 * Tests serializing, getting and rendering of the entity.
 *
 * @group pagedesigner
 */
class TargetHandlerTest extends PlainFieldHandlerTestBase {

  /**
   * Define the properties of the entity.
   *
   * @var array
   */
  protected $entityDefinition = [
    'type' => 'content',
    'name' => 'target',
    'langcode' => 'en',
    'field_content' => ['value' => '_blank'],
  ];

  /**
   * Define handler to be tested.
   *
   * @var string
   */
  protected $handlerId = 'target';

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    self::$modules[] = 'system';
    self::$modules[] = 'link';
    self::$modules[] = 'linkit';
    self::$modules[] = 'pagedesigner_link';
    parent::setUp();
    $this->installConfig(['link', 'linkit', 'pagedesigner_link']);
  }

  /**
   * {@inheritdoc}
   *
   * Additionally, assert the returned value is equal to the given value.
   */
  protected function serializeTest() {
    $result = parent::serializeTest();
    $this->assertTrue($result[0] == $this->entityDefinition['field_content']['value']);
    return $result;
  }

  /**
   * {@inheritdoc}
   *
   * Additionally, assert the returned value is equal to the given value.
   */
  protected function getTest() {
    $value = parent::getTest();
    $this->assertTrue($value == $this->entityDefinition['field_content']['value']);
    return $value;
  }

  /**
   * {@inheritdoc}
   *
   * Additionally, check the render array schema and content.
   */
  protected function internalBuildTest() {
    $build = parent::internalBuildTest();
    $this->assertTrue($build['#plain_text'] == $this->entityDefinition['field_content']['value']);
    return $build;
  }

}

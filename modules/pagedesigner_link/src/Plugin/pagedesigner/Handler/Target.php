<?php

namespace Drupal\pagedesigner_link\Plugin\pagedesigner\Handler;

use Drupal\pagedesigner\Entity\Element;
use Drupal\pagedesigner\Plugin\PlainFieldHandlerBase;

/**
 * Process entities of type "target".
 *
 * @PagedesignerHandler(
 *   id = "target",
 *   name = @Translation("Target handler"),
 *   types = {
 *      "target",
 *   },
 * )
 */
class Target extends PlainFieldHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function collectAttachments(array &$attachments) {
    $attachments['library'][] = 'pagedesigner_link/pagedesigner';
  }

  /**
   * {@inheritdoc}
   */
  public function generate($definition, array $data, Element &$entity = NULL) {
    parent::generate(['type' => 'content', 'name' => 'target'], $data, $entity);
  }

}

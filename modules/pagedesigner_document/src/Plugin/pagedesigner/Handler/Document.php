<?php

namespace Drupal\pagedesigner_document\Plugin\pagedesigner\Handler;

use Drupal\pagedesigner\Entity\Element;
use Drupal\pagedesigner_media\Plugin\MediaFieldHandlerBase;

/**
 * Process entities of type "document".
 *
 * @PagedesignerHandler(
 *   id = "document",
 *   name = @Translation("Document handler"),
 *   types = {
 *      "document"
 *   },
 * )
 */
class Document extends MediaFieldHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function collectAttachments(array &$attachments) {
    $attachments['library'][] = 'pagedesigner_document/pagedesigner';
    static::addMediaTypeInfo($attachments, 'document');
  }

  /**
   * {@inheritdoc}
   */
  public function generate($definition, array $data, Element &$entity = NULL) {
    parent::generate(['type' => 'document', 'name' => 'document'], $data, $entity);
  }

}

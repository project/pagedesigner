<?php

namespace Drupal\Tests\pagedesigner_media\Kernel\HandlerTests;

use Drupal\Tests\pagedesigner\Kernel\HandlerTests\PlainFieldHandlerTestBase;

/**
 * Base class for testing handler dealing with media.
 *
 * Tests serializing, getting and rendering of the entity.
 *
 * @group pagedesigner
 */
abstract class MediaHandlerTestBase extends PlainFieldHandlerTestBase {

  /**
   * The file entity for the test.
   *
   * @var Drupal\file\Entity\File
   */
  protected $file = NULL;

  /**
   * The media entity for the test.
   *
   * @var Drupal\media\Entity\Media
   */
  protected $media = NULL;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    self::$modules[] = 'system';
    self::$modules[] = 'file';
    self::$modules[] = 'image';
    self::$modules[] = 'media';
    self::$modules[] = 'pagedesigner_media';
    parent::setUp();
    $this
      ->installEntitySchema('file');
    $this
      ->installEntitySchema('media');
    $this
      ->installSchema('file', 'file_usage');
    $this
      ->installConfig([
        'image',
        'file',
        'media',
        'pagedesigner_media',
      ]);
  }

  /**
   * {@inheritdoc}
   *
   * Additionally, assert the returned values
   * are matching the media and the file.
   */
  protected function serializeTest() {
    $result = parent::serializeTest();
    $this->assertTrue($result['id'] == $this->media->id());
    return $result;
  }

}

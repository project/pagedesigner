<?php

namespace Drupal\pagedesigner_media\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceIdFormatter;

/**
 * An 'entity reference ID' formatter using the 'view label' permission.
 *
 * @FieldFormatter(
 *   id = "entity_reference_entity_id_view_label_permission",
 *   label = @Translation("Entity ID with 'view label' permission"),
 *   description = @Translation("Display the ID of the referenced entities using 'view label' permission."),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class EntityReferenceIdWithViewLabelPermissionFormatter extends EntityReferenceIdFormatter {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity) {
    return $entity->access('view label', NULL, TRUE);
  }

}

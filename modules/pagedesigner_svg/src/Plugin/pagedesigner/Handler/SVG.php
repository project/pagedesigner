<?php

namespace Drupal\pagedesigner_svg\Plugin\pagedesigner\Handler;

use Drupal\pagedesigner\Entity\Element;
use Drupal\pagedesigner_media\Plugin\MediaFieldHandlerBase;

/**
 * Process entities of type "svg".
 *
 * @PagedesignerHandler(
 *   id = "svg",
 *   name = @Translation("SVG handler"),
 *   types = {
 *      "svg"
 *   },
 * )
 */
class SVG extends MediaFieldHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function collectAttachments(array &$attachments) {
    $attachments['library'][] = 'pagedesigner_svg/pagedesigner';
    static::addMediaTypeInfo($attachments, 'svg');
  }

  /**
   * {@inheritdoc}
   */
  public function generate($definition, array $data, Element &$entity = NULL) {
    parent::generate(['type' => 'svg', 'name' => 'svg'], $data, $entity);
  }

}

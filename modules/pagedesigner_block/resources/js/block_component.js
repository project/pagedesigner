// components
(function ($, Drupal) {

  Drupal.behaviors.pagedesigner_block_init_components = {
    attach: function (context, settings) {
      once('pagedesigner_block_init_components', 'body', context).forEach(() => {
        $(document).on('pagedesigner-init-components', function (e, editor) {
          component(editor);
        });
      });
    }
  };
  
  // add new component type block
  function component(editor) {
    editor.DomComponents.addType('block', {
      extend: 'component',
    });
  }

})(jQuery, Drupal);

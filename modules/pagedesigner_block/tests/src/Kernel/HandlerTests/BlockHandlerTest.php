<?php

namespace Drupal\Tests\pagedesigner_block\Kernel\HandlerTests;

use Drupal\block\Entity\Block;
use Drupal\Tests\pagedesigner\Kernel\HandlerTests\CompoundHandlerTestBase;

/**
 * Test the "block" handler with a predefined entity defintion.
 *
 * Tests serializing, getting and rendering of the entity.
 *
 * @group pagedesigner
 */
class BlockHandlerTest extends CompoundHandlerTestBase {

  /**
   * Define the properties of the entity.
   *
   * @var array
   */
  protected $entityDefinition = [
    'type' => 'block',
    'name' => 'block',
    'langcode' => 'en',
    'field_block' => ['target_id' => 'test_block'],
  ];

  /**
   * Define handler to be tested.
   *
   * @var string
   */
  protected $handlerId = 'block';

  /**
   * {@inheritdoc}
   *
   * @todo Add the pagedesigner editor config to the environment.
   */
  public function setUp(): void {
    self::$modules[] = 'system';
    self::$modules[] = 'block';
    self::$modules[] = 'pagedesigner_block';
    parent::setUp();

    // Enable the stark theme and set it as default.
    \Drupal::service('theme_installer')
      ->install([
        'stark',
      ], FALSE);
    \Drupal::configFactory()
      ->getEditable('system.theme')
      ->set('default', 'stark')
      ->save();

    // Add a block to the pagedesigner region.
    Block::create(
      [
        'id' => $this->entityDefinition['field_block']['target_id'],
        'plugin' => 'system_powered_by_block',
        'region' => 'pagedesigner',
        'theme' => 'stark',
      ]
    )->save();
  }

  /**
   * {@inheritdoc}
   *
   * Additionally, assert the returned array contains the block pattern.
   */
  protected function collectPatternsTest() {
    $patterns = parent::collectPatternsTest();
    $this->assertTrue(!empty($patterns['block:' . $this->entityDefinition['field_block']['target_id']]));
    $definition = $patterns['block:' . $this->entityDefinition['field_block']['target_id']];
    $this->assertTrue($definition->getAdditional()['type'] == 'block');
    $this->assertTrue(!empty($definition->getAdditional()['markup']));
    return $patterns;
  }

  /**
   * {@inheritdoc}
   *
   * Additionally, assert the returned array contains the pagedesigner library.
   */
  protected function collectAttachmentsTest() {
    $attachments = parent::collectAttachmentsTest();
    $this->assertTrue(\in_array('pagedesigner_block/pagedesigner', $attachments['library']));
    return $attachments;
  }

  /**
   * {@inheritdoc}
   *
   * Additionally, assert the returned value is equal to the given value.
   */
  protected function serializeTest() {
    $result = parent::serializeTest();
    $this->assertTrue($result['block_id'] == $this->entityDefinition['field_block']['target_id']);
    return $result;
  }

  /**
   * {@inheritdoc}
   *
   * Additionally, assert the returned value is equal to the given value.
   */
  protected function getTest() {
    $value = parent::getTest();
    $this->assertTrue($value == $this->entityDefinition['field_block']['target_id']);
    return $value;
  }

  /**
   * {@inheritdoc}
   *
   * Additionally, check the render array schema and content.
   */
  protected function internalViewTest() {
    $build = parent::internalViewTest();
    $this->assertTrue(!empty($build['#lazy_builder']));
    return $build;
  }

}

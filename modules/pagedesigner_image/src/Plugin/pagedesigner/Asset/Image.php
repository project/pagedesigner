<?php

namespace Drupal\pagedesigner_image\Plugin\pagedesigner\Asset;

use Drupal\Core\Form\FormState;
use Drupal\pagedesigner\Plugin\pagedesigner\Asset\Standard;
use Drupal\views\Form\ViewsExposedForm;
use Drupal\views\Views;

/**
 * Process assets of type "image".
 *
 * @PagedesignerAsset(
 *   id = "image",
 *   name = @Translation("Image asset"),
 *   types = {
 *      "image",
 *   },
 * )
 */
class Image extends Standard {

  /**
   * {@inheritdoc}
   */
  public function getSearchForm() {
    $view = Views::getView('image');
    $view->setDisplay('asset');
    $view->initHandlers();
    $form_state = new FormState();
    $form_state->setFormState([
      'view' => $view,
      'display' => $view->display_handler->display,
      'exposed_form_plugin' => $view->display_handler->getPlugin('exposed_form'),
      'method' => 'get',
      'rerender' => TRUE,
      'no_redirect' => TRUE,
      'always_process' => TRUE,
    ]);
    $form = \Drupal::formBuilder()->buildForm(ViewsExposedForm::class, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreateForm() {
    $values = ['bundle' => 'image'];
    $node = \Drupal::entityTypeManager()
      ->getStorage('media')
      ->create($values);
    $form = \Drupal::entityTypeManager()
      ->getFormObject('media', 'default')
      ->setEntity($node);
    return \Drupal::formBuilder()->getForm($form);
  }

}

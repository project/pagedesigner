<?php

namespace Drupal\Tests\pagedesigner_image\Kernel\HandlerTests;

use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use Drupal\Tests\pagedesigner_media\Kernel\HandlerTests\MediaHandlerTestBase;

/**
 * Test the "image" handler with a predefined entity defintion.
 *
 * Tests serializing, getting and rendering of the entity.
 *
 * @group pagedesigner
 */
class ImageHandlerTest extends MediaHandlerTestBase {

  /**
   * Define the properties of the entity.
   *
   * @var array
   */
  protected $entityDefinition = [
    'type' => 'image',
    'name' => 'image',
    'langcode' => 'en',
    'field_media' => ['target_id' => 1],
  ];

  /**
   * Define handler to be tested.
   *
   * @var string
   */
  protected $handlerId = 'image';

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    self::$modules[] = 'pagedesigner_image';
    parent::setUp();
    $this->installConfig(['pagedesigner_image']);
  }

  /**
   * {@inheritdoc}
   */
  public function assertPreConditions(): void {
    parent::assertPreConditions();

    // Create file entity.
    $this->file = File::create([
      'filename' => 'test.jpg',
      'uri' => 'public://page/test.jpg',
      'status' => 1,
    ]);
    $this->file->save();

    // Create media entity.
    $this->media = Media::create([
      'bundle' => $this->entityDefinition['type'],
      'field_media_image' => ['entity' => $this->file],
    ]);
    $this->entity->field_media->entity = $this->media;
    $this->entityDefinition['field_media']['target_id'] = $this->media->id();
    $this->assertTrue($this->entity->field_media->entity == $this->media);
    $this->assertTrue($this->entityDefinition['field_media']['target_id'] == $this->media->id());
  }

  /**
   * {@inheritdoc}
   *
   * Additionally, assert the returned array contains the pagedesigner library.
   */
  protected function collectAttachmentsTest() {
    $attachments = parent::collectAttachmentsTest();
    $this->assertTrue(\in_array('pagedesigner_image/pagedesigner', $attachments['library']));
    return $attachments;
  }

  /**
   * {@inheritdoc}
   *
   * Additionally, assert the returned value is equal to the file url.
   */
  protected function getTest() {
    $url = NULL;
    $value = parent::getTest();
    /** @var \Drupal\image\Entity\ImageStyle $style */
    $style = \Drupal::entityTypeManager()
      ->getStorage('image_style')
      ->load('pagedesigner_default');
    $url = $style->buildUrl($this->file->getFileUri());
    $this->assertTrue($value == $url);
    return $value;
  }

  /**
   * {@inheritdoc}
   *
   * Additionally, assert the returned values
   * are matching the media and the file.
   */
  protected function serializeTest() {
    $result = parent::serializeTest();
    $this->assertTrue($result['src'] == $this->file->createFileUrl());
    return $result;
  }

  /**
   * {@inheritdoc}
   *
   * Additionally, check the render array schema and content.
   */
  protected function internalBuildTest() {
    $url = NULL;
    $build = parent::internalBuildTest();
    /** @var \Drupal\image\Entity\ImageStyle $style */
    $style = \Drupal::entityTypeManager()
      ->getStorage('image_style')
      ->load('pagedesigner_default');
    if ($style != NULL) {
      $url = $style->buildUrl($this->file->getFileUri());
    }
    $this->assertTrue($build['#plain_text'] == $url);
    return $build;
  }

}

<?php

namespace Drupal\pagedesigner_audio\Plugin\pagedesigner\Handler;

use Drupal\pagedesigner\Entity\Element;
use Drupal\pagedesigner_media\Plugin\MediaFieldHandlerBase;

/**
 * Process entities of type "audio".
 *
 * @PagedesignerHandler(
 *   id = "audio",
 *   name = @Translation("Audio handler"),
 *   types = {
 *      "audio"
 *   },
 * )
 */
class Audio extends MediaFieldHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function collectAttachments(array &$attachments) {
    $attachments['library'][] = 'pagedesigner_audio/pagedesigner';
    static::addMediaTypeInfo($attachments, 'audio');
  }

  /**
   * {@inheritdoc}
   */
  public function generate($definition, array $data, Element &$entity = NULL) {
    parent::generate(['type' => 'audio', 'name' => 'audio'], $data, $entity);
  }

}

<?php

namespace Drupal\Tests\pagedesigner\Kernel\HandlerTests;

use Drupal\Component\Utility\Xss;

/**
 * Test the "text" handler with a predefined entity defintion.
 *
 * Tests serializing, getting and rendering of the entity.
 *
 * @group pagedesigner
 */
class TextHandlerTest extends FieldHandlerTestBase {

  /**
   * Define the properties of the entity.
   *
   * @var array
   */
  protected $entityDefinition = [
    'type' => 'content',
    'name' => 'content',
    'langcode' => 'en',
    'field_content' => [
      'value' => 'an_option_value',
    ],
  ];

  /**
   * Define handler to be tested.
   *
   * @var string
   */
  protected $handlerId = 'text';

  /**
   * {@inheritdoc}
   *
   * Additionally, assert the returned value is equal to the given value.
   */
  protected function serializeTest() {
    $result = parent::serializeTest();
    $this->assertTrue($result[0] == Xss::filter($this->entityDefinition['field_content']['value']));
    return $result;
  }

  /**
   * {@inheritdoc}
   *
   * Additionally, assert the returned value is equal to the given value.
   */
  protected function getTest() {
    $value = parent::getTest();
    $this->assertTrue($value == Xss::filter($this->entityDefinition['field_content']['value']));
    return $value;
  }

  /**
   * {@inheritdoc}
   *
   * Additionally, check the render array schema and content.
   */
  protected function internalBuildTest() {
    $build = parent::internalBuildTest();
    $this->assertTrue($build['#markup'] == Xss::filter($this->entityDefinition['field_content']['value']));
    return $build;
  }

}

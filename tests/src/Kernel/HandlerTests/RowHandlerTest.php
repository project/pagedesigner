<?php

namespace Drupal\Tests\pagedesigner\Kernel\HandlerTests;

/**
 * Test the "row" handler with a predefined entity defintion.
 *
 * Tests serializing, getting and rendering of the entity.
 *
 * @group pagedesigner
 */
class RowHandlerTest extends CompoundHandlerTestBase {

  /**
   * Define the properties of the entity.
   *
   * @var array
   */
  protected $entityDefinition = [
    'type' => 'row',
    'name' => 'row',
    'langcode' => 'en',
    'field_pattern' => ['value' => 'test_pattern'],
  ];

  /**
   * Define handler to be tested.
   *
   * @var string
   */
  protected $handlerId = 'row';

  /**
   * {@inheritDoc}
   */
  protected function internalViewTest() {
    $build = parent::internalViewTest();
    $this->assertTrue(!empty($build['#pre_render']));
    return $build;
  }

  /**
   * {@inheritDoc}
   */
  protected function internalBuildTest() {
    $build = parent::internalBuildTest();

    // Assert that the build array is correctly populated.
    $this->assertEquals('pattern', $build['pagedesigner_structure']['#type']);
    $this->assertEquals('test_pattern', $build['pagedesigner_structure']['#id']);
    $this->assertEquals([], $build['pagedesigner_structure']['#fields']);

    // Assert that the post_render callback is added.
    $this->assertContains([$this->viewBuilder, 'postRender'], $build['pagedesigner_structure']['#post_render']);

    // Assert that the pre_render key is unset.
    $this->assertFalse(isset($build['pagedesigner_structure']['#pre_render']));

    // Assert that the build array is wrapped in 'pagedesigner_structure'.
    $this->assertEquals(['pagedesigner_structure' => $build['pagedesigner_structure']], $build);

    return $build;
  }

}

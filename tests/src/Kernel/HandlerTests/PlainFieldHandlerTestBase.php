<?php

namespace Drupal\Tests\pagedesigner\Kernel\HandlerTests;

/**
 * Base class to test a field handler with a predefined entity defintion.
 *
 * Tests serializing, getting and rendering of the entity.
 *
 * @group pagedesigner
 */
abstract class PlainFieldHandlerTestBase extends HandlerTestBase {

  /**
   * Test the render function of the handler.
   *
   * @return array
   *   The produced render representation of the entity.
   */
  protected function editBuildTest() {
    $build = parent::editBuildTest();
    $this->assertTrue(isset($build['#plain_text']));
    return $build;
  }

}

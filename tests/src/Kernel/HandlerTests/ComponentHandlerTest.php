<?php

namespace Drupal\Tests\pagedesigner\Kernel\HandlerTests;

/**
 * Test the "row" handler with a predefined entity defintion.
 *
 * Tests serializing, getting and rendering of the entity.
 *
 * @group pagedesigner
 */
class ComponentHandlerTest extends CompoundHandlerTestBase {

  /**
   * Define the properties of the entity.
   *
   * @var array
   */
  protected $entityDefinition = [
    'type' => 'component',
    'name' => 'component',
    'langcode' => 'en',
  ];

  /**
   * Define handler to be tested.
   *
   * @var string
   */
  protected $handlerId = 'component';

  /**
   * {@inheritDoc}
   */
  protected function internalViewTest() {
    $build = parent::internalViewTest();
    $this->assertTrue(!empty($build['#lazy_builder']));
    $this->assertCount(2, $build);
    return $build;
  }

}

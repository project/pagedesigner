<?php

namespace Drupal\Tests\pagedesigner\Kernel\HandlerTests;

/**
 * Test the "hide" handler with a predefined entity defintion.
 *
 * Tests serializing, getting and rendering of the entity.
 *
 * @group pagedesigner
 */
class HideHandlerTest extends PlainFieldHandlerTestBase {

  /**
   * Define the properties of the entity.
   *
   * @var array
   */
  protected $entityDefinition = [
    'type' => 'content',
    'name' => 'content',
    'langcode' => 'en',
    'field_content' => ['value' => 'an_option_value'],
  ];

  /**
   * Define handler to be tested.
   *
   * @var string
   */
  protected $handlerId = 'hide';

  /**
   * {@inheritdoc}
   *
   * Additionally, assert the returned value is equal to the given value.
   */
  protected function serializeTest() {
    $result = parent::serializeTest();
    $this->assertTrue(count($result) == 0);
    return $result;
  }

  /**
   * {@inheritdoc}
   *
   * Additionally, assert the returned value is equal to the given value.
   */
  protected function getTest() {
    $value = parent::getTest();
    $this->assertTrue(empty($value));
    return $value;
  }

}

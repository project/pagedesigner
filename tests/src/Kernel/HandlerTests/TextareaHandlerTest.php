<?php

namespace Drupal\Tests\pagedesigner\Kernel\HandlerTests;

/**
 * Test the "longtext" handler with a predefined entity defintion.
 *
 * Tests serializing, getting and rendering of the entity.
 *
 * @group pagedesigner
 */
class TextareaHandlerTest extends LongtextHandlerTest {

  /**
   * Define handler to be tested.
   *
   * @var string
   */
  protected $handlerId = 'textarea';

}

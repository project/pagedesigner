<?php

namespace Drupal\Tests\pagedesigner\Kernel\HandlerTests;

use Drupal\Core\Render\Markup;

/**
 * Test the "longtext" handler with a predefined entity defintion.
 *
 * Tests serializing, getting and rendering of the entity.
 *
 * @group pagedesigner
 */
class LongtextHandlerTest extends EditorHandlerTestCase {

  /**
   * Define the properties of the entity.
   *
   * @var array
   */
  protected $entityDefinition = [
    'type' => 'content',
    'name' => 'content',
    'langcode' => 'en',
    'field_content' => ['value' => '<style>.test{}</style>some html'],
  ];

  /**
   * Define handler to be tested.
   *
   * @var string
   */
  protected $handlerId = 'longtext';

  /**
   * {@inheritdoc}
   *
   * Additionally, assert the returned value is equal to the given value.
   */
  protected function getTest() {
    $value = parent::getTest();
    $this->assertTrue($value == Markup::create($this->entityDefinition['field_content']['value']));
    return $value;
  }

}

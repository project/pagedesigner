<?php

namespace Drupal\Tests\pagedesigner\Kernel\HandlerTests;

use Drupal\pagedesigner\ElementViewBuilder;

/**
 * Test the "html" handler with a predefined entity defintion.
 *
 * Tests serializing, getting and rendering of the entity.
 *
 * @group pagedesigner
 */
class HTMLHandlerTest extends EditorHandlerTestCase {

  /**
   * Define the properties of the entity.
   *
   * @var array
   */
  protected $entityDefinition = [
    'type' => 'content',
    'name' => 'content',
    'langcode' => 'en',
    'field_content' =>
    [
      'value' => '<p>some test data</p>',
    ],
  ];

  /**
   * Define handler to be tested.
   *
   * @var string
   */
  protected $handlerId = 'html';

  /**
   * {@inheritdoc}
   *
   * Additionally, assert the returned value is equal to the given value.
   */
  protected function getTest() {
    $value = parent::getTest();
    $build = [];
    $this->handler->view($this->entity, ElementViewBuilder::RENDER_MODE_INTERNAL, $build);
    $this->handler->build($this->entity, ElementViewBuilder::RENDER_MODE_INTERNAL, $build);
    $value = \Drupal::service('renderer')->renderPlain($build);
    return $value;
  }

}

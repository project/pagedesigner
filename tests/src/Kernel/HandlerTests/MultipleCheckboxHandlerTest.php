<?php

namespace Drupal\Tests\pagedesigner\Kernel\HandlerTests;

/**
 * Test the "multiplecheckbox" handler with a predefined entity defintion.
 *
 * Tests serializing, getting and rendering of the entity.
 *
 * @group pagedesigner
 */
class MultipleCheckboxHandlerTest extends HandlerTestBase {

  /**
   * {@inheritdoc}
   */
  protected $handlerId = 'multiplecheckbox';

  /**
   * {@inheritdoc}
   */
  protected $entityDefinition = [
    'type' => 'content',
    'name' => 'multiplecheckbox',
    'langcode' => 'en',
    'field_content' => ['value' => '[]'],
  ];

  /**
   * {@inheritdoc}
   */
  protected $fieldDefinition = [
    'name' => 'multiplecheckbox',
    'label' => 'multiplecheckbox',
    'type' => 'multiplecheckbox',
    'options' => ['option1', 'option2'],
    'value' => ['value1', 'value2'],
  ];

  /**
   * {@inheritdoc}
   */
  public function testPrepare() {
    // Call the prepare method.
    $fieldArray = parent::prepareTest();

    // Assert that the options and values have been prepared correctly.
    $this->assertEquals([
      'options' => ['option1', 'option2'],
      'values' => ['value1', 'value2'],
    ], $fieldArray);

    return $fieldArray;
  }

}

<?php

namespace Drupal\Tests\pagedesigner\Kernel\HandlerTests;

/**
 * Tests editor handler with a predefined entity defintion.
 *
 * @group pagedesigner
 */
abstract class EditorHandlerTestCase extends HandlerTestBase {

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    self::$modules[] = 'editor';
    parent::setUp();
  }

  /**
   * {@inheritdoc}
   *
   * Additionally, assert the returned value is equal to the given value.
   */
  protected function serializeTest() {
    $result = parent::serializeTest();
    $this->assertTrue($result[0] == $this->entityDefinition['field_content']['value']);
    return $result;
  }

  /**
   * {@inheritdoc}
   *
   * Additionally, assert the returned value is equal to the given value.
   */
  protected function getTest() {
    $value = parent::getTest();
    return $value;
  }

  /**
   * {@inheritdoc}
   *
   * Additionally, check the render array schema and content.
   */
  protected function internalBuildTest() {
    $build = parent::internalBuildTest();
    $testBuild = [
      '#type' => 'processed_text',
      '#text' => $this->entityDefinition['field_content']['value'],
      '#format' => 'plain_text',
      '#filter_types_to_skip' => [],
      '#langcode' => $this->entityDefinition['langcode'],
    ];
    $this->assertTrue($build['#type'] == $testBuild['#type']);
    $this->assertTrue($build['#text'] == $testBuild['#text']);
    $this->assertTrue($build['#format'] == $testBuild['#format']);
    $this->assertTrue($build['#filter_types_to_skip'] == $testBuild['#filter_types_to_skip']);
    $this->assertTrue($build['#langcode'] == $testBuild['#langcode']);
    return $build;
  }

}

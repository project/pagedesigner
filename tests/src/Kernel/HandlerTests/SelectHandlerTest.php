<?php

namespace Drupal\Tests\pagedesigner\Kernel\HandlerTests;

/**
 * Test the "select" handler with a predefined entity defintion.
 *
 * Tests serializing, getting and rendering of the entity.
 *
 * @group pagedesigner
 */
class SelectHandlerTest extends PlainFieldHandlerTestBase {

  /**
   * Define the properties of the entity.
   *
   * @var array
   */
  protected $entityDefinition = [
    'type' => 'content',
    'name' => 'content',
    'langcode' => 'en',
    'field_content' => ['value' => 'an_option_value'],
  ];

  /**
   * Define handler to be tested.
   *
   * @var string
   */
  protected $handlerId = 'select';

  /**
   * {@inheritdoc}
   *
   * Additionally, assert the returned value is equal to the given value.
   */
  protected function serializeTest() {
    $result = parent::serializeTest();
    $this->assertTrue($result[0] == $this->entityDefinition['field_content']['value']);
    return $result;
  }

  /**
   * {@inheritdoc}
   *
   * Additionally, assert the returned value is equal to the given value.
   */
  protected function getTest() {
    $value = parent::getTest();
    $this->assertTrue($value == $this->entityDefinition['field_content']['value']);
    return $value;
  }

  /**
   * {@inheritdoc}
   *
   * Additionally, check the render array schema and content.
   */
  protected function internalBuildTest() {
    $build = parent::internalBuildTest();
    $this->assertTrue($build['#plain_text'] == $this->entityDefinition['field_content']['value']);
    return $build;
  }

}

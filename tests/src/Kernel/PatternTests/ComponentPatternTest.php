<?php

namespace Drupal\Tests\pagedesigner\Kernel\PatternTests;

use Drupal\editor\Entity\Editor;

/**
 * Test the core handlers with a predefined, virtual pattern.
 *
 * @group pagedesigner
 */
class ComponentPatternTest extends PatternTestBase {
  /**
   * The pattern ids.
   *
   * @var string
   */
  protected $patternIds = ['component_test'];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    self::$modules[] = 'system';
    self::$modules[] = 'editor';
    self::$modules[] = 'filter';
    self::$modules[] = 'ckeditor';

    parent::setUp();

    $this->installConfig(['system']);
    $this->installConfig(['editor']);
    $this->installConfig(['filter']);
    $this->installConfig(['ckeditor']);
  }

  /**
   * {@inheritdoc}
   */
  public function assertPreConditions(): void {
    parent::assertPreConditions();
    $editor = Editor::create([
      'id' => 'plain_text',
      'format' => 'plain_text',
      'editor' => 'ckeditor',
    ]);
    $editor->save();
  }

}

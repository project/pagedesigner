<?php

namespace Drupal\Tests\pagedesigner\Kernel\PatternTests;

use Drupal\pagedesigner\Entity\Element;

/**
 * Test the core handlers with a predefined, virtual pattern.
 *
 * Tests generation, describing and serializing and deletion functions of
 * the involved handlers.
 *
 * @group pagedesigner
 */
class RowPatternTest extends PatternTestBase {
  /**
   * The pattern ids.
   *
   * @var string
   */
  protected $patternIds = ['row_test'];

  /**
   * {@inheritdoc}
   */
  public function assertPreConditions(): void {
    parent::assertPreConditions();
    $entity = Element::create(
      [
        'type' => 'content',
        'name' => 'test_cell1',
        'langcode' => 'en',
      ]);
    $entity->save();
    $id1 = $entity->id();

    $entity = Element::create(
      [
        'type' => 'content',
        'name' => 'test_cell2',
        'langcode' => 'en',
      ]);
    $entity->save();
    $id2 = $entity->id();
    $entity = Element::create(
      [
        'type' => 'content',
        'name' => 'test_cell1',
        'langcode' => 'en',
      ]);
    $entity->save();
    $id3 = $entity->id();

    $entity = Element::create(
      [
        'type' => 'content',
        'name' => 'test_cell2',
        'langcode' => 'en',
      ]);
    $entity->save();
    $id4 = $entity->id();

    $this->patchData = [
      'fields' => [
        'column0' => [
          'order' =>
          [
            $id1,
            $id2,
          ],
        ],
        'column1' => [
          'order' =>
          [
            $id3,
            $id4,
          ],
        ],
      ],
    ] + $this->patchData;
  }

  /**
   * {@inheritdoc}
   */
  protected function descriptionTest() {
    $description = parent::descriptionTest();
    $this->assertTrue($description['fields'][0]['type'] == 'cell');
    $this->assertTrue($description['fields'][1]['type'] == 'cell');
    return $description;
  }

  /**
   * {@inheritdoc}
   */
  protected function serializationTestWithData() {
    $serialization = parent::serializationTestWithData();
    $this->assertTrue($serialization['fields']['column0'][0] == $this->patchData['fields']['column0']['order'][0]);
    $this->assertTrue($serialization['fields']['column0'][1] == $this->patchData['fields']['column0']['order'][1]);
    $this->assertTrue($serialization['fields']['column1'][0] == $this->patchData['fields']['column1']['order'][0]);
    $this->assertTrue($serialization['fields']['column1'][1] == $this->patchData['fields']['column1']['order'][1]);
    return $serialization;
  }

}

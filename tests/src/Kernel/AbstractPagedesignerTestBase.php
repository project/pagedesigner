<?php

namespace Drupal\Tests\pagedesigner\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Base class for Kernel Tests.
 *
 * @group pagedesigner
 */
abstract class AbstractPagedesignerTestBase extends KernelTestBase {
  use UserCreationTrait;

  /**
   * Enable the necessary modules for testing.
   *
   * @var array
   */
  protected static $modules = [
    'pagedesigner',
    'ui_patterns',
    'user',
    'node',
    'field',
    'taxonomy',
    'text',
    'editor',
    'rest',
    'serialization',
  ];

  /**
   * The base entity created by the test.
   *
   * @var \Drupal\pagedesigner\Entity\Element
   */
  protected $entity = NULL;

  /**
   * Set up environment for Kernel testing.
   *
   * Set up current user, install config and schema for pagedesigner elements.
   */
  public function setUp(): void {
    parent::setUp();
    $this->setUpCurrentUser();
    $this->installEntitySchema('pagedesigner_element');
    $this->installConfig(['pagedesigner']);
  }

}

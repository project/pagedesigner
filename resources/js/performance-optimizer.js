(function ($, Drupal) {


  Drupal.behaviors.pagedesigner_optimize_performance_start = {
    attach: function (context, settings) {
      once('pagedesigner_optimize_performance_start', 'body', context).forEach(() => {
        $(document).on('pagedesigner-before-setup', function (e, editor, options) {

          window.exclude_dom_elements_before = [];
          window.exclude_dom_elements_after = [];

          // remove uneditable elements from DOM and store them
          if (drupalSettings.pagedesigner.exclude_dom_elements_before) {
            drupalSettings.pagedesigner.exclude_dom_elements_before.split("\n").forEach(function (selector, index) {
              if (selector) {
                document.querySelectorAll(selector).forEach(function (element) {
                  window.exclude_dom_elements_before.push(element.cloneNode(true));
                  element.parentNode.removeChild(element);
                });
              }
            });
          }

          if (drupalSettings.pagedesigner.exclude_dom_elements_after) {
            drupalSettings.pagedesigner.exclude_dom_elements_after.split("\n").forEach(function (selector, index) {
              if (selector) {
                document.querySelectorAll(selector).forEach(function (element) {
                  window.exclude_dom_elements_after.push(element.cloneNode(true));
                  element.parentNode.removeChild(element);
                });
              }
            });
          }
        });
      });
    }
  };

  Drupal.behaviors.pagedesigner_optimize_performance_end = {
    attach: function (context, settings) {
      once('pagedesigner_optimize_performance_end', 'body', context).forEach(() => {
        $(document).on('pagedesigner-after-init', function () {
          reattachElementsWhenCanvasReady(editor);

          // remove loading screen after iframe is ready
          $('iframe.gjs-frame').ready(function () {
            setTimeout(function () {
              $('#pagedesigner-loading-screen-container').fadeOut(300, function () {
                $(this).remove();
              });
            }, 500);
            editor.runCommand('sw-visibility');
            const showBorders = editor.Panels.getButton('options', 'sw-visibility');
            showBorders && showBorders.set('active', true);
          });
        });
      });
    }
  }

  // Reattaches uneditable elements that have been removed earlier
  function reattachElements(editor) {
    var wrapper = editor.Canvas.getDocument().getElementById('wrapper');
    window.exclude_dom_elements_before.forEach(function (element) {
      wrapper.parentNode.insertBefore(element, wrapper);
    });
    delete window.exclude_dom_elements_before;

    window.exclude_dom_elements_after.reverse().forEach(function (element) {
      wrapper.parentNode.insertBefore(element, wrapper.nextSibling);
    });
    delete window.exclude_dom_elements_after;
  }

  // Continuously check if canvas is ready
  function reattachElementsWhenCanvasReady(editor) {
    if (editor.Canvas.getDocument().readyState == 'complete') {
      reattachElements(editor);
      return;
    }

    window.setTimeout(function () {
      reattachElementsWhenCanvasReady(editor);
    }, 50);
  }

})(jQuery, Drupal);

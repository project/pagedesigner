(function ($, Drupal) {

  function init(editor) {
    const TraitManager = editor.TraitManager;
    TraitManager.addType('textarea', Object.assign({}, TraitManager.defaultTrait, {
      getInputEl: function () {
        if (!this.inputEl) {
          var value = this.model.getTargetValue();
          var input = jQuery('<textarea>' + value + '</textarea>');
          input.attr('rows', 20);
          this.inputEl = input.get(0);
        }
        return this.inputEl;
      },
      getRenderValue: function (value) {
        if (Array.isArray(value)) {
          value = value[0];
        }
        return value.replace(/<\!--/g, "").replace(/\/\/-->/g, "").replace(/-->/g, "");;
      }
    }));
    TraitManager.addType('longtext', Object.assign({}, TraitManager.getType('textarea')),{

      getRenderValue: function (value) {
        if (Array.isArray(value)) {
          value = value[0];
        }
        return value.replace(/\n/g, "<br />");
      }
    });
  }

  Drupal.behaviors.pagedesigner_trait_textarea = {
    attach: function (context, settings) {
      $(document).on('pagedesigner-init-traits', function (e, editor) {
        init(editor);
      });
    }
  };

})(jQuery, Drupal);

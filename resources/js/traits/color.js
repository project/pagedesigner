(function ($, Drupal) {

  function init(editor) {
    editor.TraitManager.addType('color', Object.assign({}, editor.TraitManager.defaultTrait));
  }

  Drupal.behaviors.pagedesigner_trait_color = {
    attach: function (context, settings) {
      $(document).on('pagedesigner-init-traits', function (e, editor) {
        init(editor);
      });
    }
  };

})(jQuery, Drupal);

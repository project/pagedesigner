(function ($, Drupal) {

  function init(editor) {
    const TraitManager = editor.TraitManager;
    TraitManager.addType('multiplecheckbox',
      Object.assign({}, TraitManager.defaultTrait,
        {
          events: {
            click: 'handleClick'
          },

          afterInit: function () {
            var value = this.model.get('value');
            if ((!value || value == '')) {
              if (this.model.attributes.additional.preview) {
                this.model.set('value', this.model.attributes.additional.preview);
              }
            }
          },
          handleClick: function (e) {
            let value = {};
            if (this.model.get('value')) {
              value = this.model.get('value');
            }
            let $input = $(e.target).parent().find('input');
            if ($input.is(':checked')) {
              value[String($input.val())] = $input.val();
            }
            else {
              delete value[$input.val()];
            }
            this.model.set('value', value);
          },


          getInputEl: function () {
            if (!this.inputEl) {

              let traitName = this.model.get('name');

              let value = {};
              if (this.target.attributes.attributes[traitName]) {
                value = this.target.attributes.attributes[traitName];
              }

              let options = this.model.attributes.additional.options;

              let $traitElement = $('<div class="gjs-field-multiplecheckbox-options">');

              for (var key in options) {
                let $optionContainer = $('<div></div>');
                let elementId = 'multiplecheckbox-' + traitName + '-' + key;

                let $option = $('<input type="checkbox" name="' + traitName + '[]" value="' + key + '" id="' + elementId + '">');
                let $label = $('<label for="' + elementId + '">' + options[key] + '</label>');

                if (key in value && value[key]) {
                  $option.prop('checked', true)
                }

                $optionContainer.append($option);
                $optionContainer.append($label);
                $traitElement.append($optionContainer);
              }
              this.inputEl = $traitElement.get(0);
            }
            return this.inputEl;
          },
          getRenderValue: function (value) {
            if (typeof this.model.get('value') == 'undefined') {
              return value;
            }
            return this.model.get('value');
          },
          setTargetValue: function (value) {
            this.model.set('value', value);
          },
          setInputValue: function (value) {
            if (value) {
              if (Array.isArray(value)) {
                let tmp = {};
                for (var x in value) {
                  tmp[String(x)] = value[x];
                }
                value = tmp;
              }
              this.model.set('value', value);
              var i;
              var options = $(this.inputEl)[0].getElementsByTagName('input');
              for (i = 0; i < options.length; i++) {
                if (value[options[i].value] == true || value[options[i].value] == options[i].value) {
                  options[i].checked = true;
                }
                else {
                  options[i].checked = false;
                }
              }
            } else {
              this.model.set('value', {});
            }
          }
        })
    );
  }

  Drupal.behaviors.pagedesigner_trait_multiplecheckbox = {
    attach: function (context, settings) {
      $(document).on('pagedesigner-init-traits', function (e, editor) {
        init(editor);
      });
    }
  };

})(jQuery, Drupal);

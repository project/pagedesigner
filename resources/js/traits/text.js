(function ($, Drupal) {

  function init(editor) {

    var textTrait = Object.assign({}, editor.TraitManager.defaultTrait);
    textTrait.getMetaData = function getMetaData(){
      if( !this.model.get('value') || ( this.model.get('value') && !this.model.get('value')[0] ) ){
        return '';
      }

      var trait = this;
      var $btnRemove = $('<button title="' + Drupal.t('Clear text') + '"></button>');
      $btnRemove.click(function(){
        trait.$input.val('');
        trait.model.set('value', ['']);
        trait.getMetaData()
      });
      return $btnRemove[0];
    };
    editor.TraitManager.addType('text', textTrait );
  }

  Drupal.behaviors.pagedesigner_trait_text = {
    attach: function (context, settings) {
      $(document).on('pagedesigner-init-traits', function (e, editor) {
        init(editor);
      });
    }
  };

})(jQuery, Drupal);

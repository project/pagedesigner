<?php

/**
 * @file
 * Hooks related to Pagedesigner module.
 */

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Session\AccountInterface;

// phpcs:disable DrupalPractice.CodeAnalysis.VariableAnalysis.UnusedVariable

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter a css statement rendered in \Drupal\pagedesigner\Service\Render\Styles.
 *
 * @param string $css
 *   The css statement.
 * @param array $context
 *   The context, in the form ['key' => $key, 'id' => $id].
 */
function hook_pagedesigner_css_statement_alter(string &$css, array $context) {
  // Remove all css for element "1234".
  if ($context['id'] == 1234) {
    $css = '';
  }
}

/**
 * Alter the css output rendered in \Drupal\pagedesigner\Service\Renderer.
 *
 * @param string $css
 *   The css statement.
 * @param array $context
 *   The context, in the form ['key' => $key].
 */
function hook_pagedesigner_css_rendered_alter(string &$css, array $context) {
  // Remove all css for key "large".
  if ($context['key'] == 'large') {
    $css = '';
  }
}

/**
 * Control pagedesigner edit access.
 *
 * @param \Drupal\Core\Entity\ContentEntityInterface $entity
 *   The entity being accessed.
 * @param \Drupal\Core\Session\AccountInterface $account
 *   The user accessing the entity.
 */
function hook_pagedesigner_access(ContentEntityInterface $entity, AccountInterface $account) {
  // Forbid access to pagedesigner.
  return AccessResult::forbidden();
}
